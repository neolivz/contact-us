import { createAsyncAction, createSimpleAction } from 'redux-async-action-reducer'
import { TOGGLE_DIALOG, TOGGLE_DONE_DIALOG, SAVE_DETAILS_REDUCER } from '../constants'
import { saveDetails as saveDetailsAPI } from '../apis/apis'


export const toggleDialog = createSimpleAction(TOGGLE_DIALOG)
export const openDialog = () => toggleDialog(true)
export const closeDialog = () => toggleDialog(false)
export const toggleDoneDialog = createSimpleAction(TOGGLE_DONE_DIALOG)
export const openDoneDialog = () => toggleDoneDialog(true)
export const closeDoneDialog = () => toggleDoneDialog(false)
export const saveDetails = createAsyncAction(SAVE_DETAILS_REDUCER, saveDetailsAPI)