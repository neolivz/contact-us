import React from 'react'
import { Field, reduxForm } from 'redux-form'
import AriaModal from 'react-aria-modal'
import styled from 'styled-components'

const Root = styled.div`
  border: 1px solid black;
  background-color: #fff;
  padding: 30px 50px 30px 50px;
`

const Button = styled.button`
  width: 100%;
`



const AllDone = props => {
  return (
        props.dialogOpen ? (<AriaModal
          titleText="demo one"
          onExit={this.deactivateModal}
          getApplicationNode={this.getApplicationNode}
          underlayStyle={{ paddingTop: '2em' }}
        >
            <Root>
              Thank you :)
              <div>
                <Button onClick={props.closeDialog}>Done</Button>
              </div>
            </Root>
      </AriaModal>) : null
    
  )
}

export default AllDone;