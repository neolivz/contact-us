import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form'
import {Row, Col, Grid} from 'react-styled-flexboxgrid'
import {Root, AppHeader, AppFooter, AppBody, Title, FooterContent, BodyContent, SignUpButton, ContentRow} from './AppStyle'

import ContactForm from '../contactUsForm'
import AllDone from '../allDone'


class App extends Component {
  submit = (values) => {
    // print the form values to the console
    this.props.saveDetails(values)
  }
  componentWillReceiveProps(newProps) {
    if(newProps.completed && !this.props.completed ){
      this.props.openDoneDialog()
    }
    if(!newProps.working && this.props.working ){
      this.props.closeDialog()
    }
  }
  render() {
    return (
      <Root>
        <ContactForm dialogOpen={this.props.dialogOpen}  working={this.props.working} onSubmit={this.submit}/>
        <AllDone dialogOpen={this.props.completed && this.props.doneDialogOpen} closeDialog={this.props.closeDoneDialog}/>
        <AppHeader>
          <Title> Our Company :) </Title>
        </AppHeader>
        <AppBody>
          <BodyContent>
            <ContentRow center='xs'>
              <Col>
                Welcome to our page :)
              </Col>
            </ContentRow>
            <ContentRow center='xs'>
              <Col>
                We are not yet ready. We will inform you when we are ready :)
              </Col>
            </ContentRow>
            <ContentRow center='xs'>
              <Col>
                <SignUpButton onClick={this.props.openDialog}> Sign Up!! </SignUpButton>
              </Col>
            </ContentRow>
          </BodyContent>
        </AppBody>
        <AppFooter>
            <FooterContent>
              Thank you for visiting :)
            </FooterContent>
        </AppFooter>
      </Root>
    );
  }
}

export default App;
