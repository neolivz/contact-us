import styled from 'styled-components'
import {Row, Col, Grid} from 'react-styled-flexboxgrid'


export const Root = styled(Grid)`
  height: 100vh;
  width: 100vw;
  background-color: red;
`

export const AppHeader = styled.header`
  height: 64px;
  width: 100vw;
  background-color: #03A9F4;
  box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
`

export const Title = styled.h1`
  font-size: 32px;
  margin: 0;
  padding-left: 1em;
  padding-top: 14px;
  padding-bottom: 14px;
  color: #fff;
`


export const AppBody = styled.div`
  width: 100vw;
  height: calc(100vh - 112px);
  background-color: #fff;
`

export const ContentRow = styled(Row)`
  padding-bottom: 0.5em;
`

export const BodyContent = styled(Row)`
  flex-direction: column;
  justify-content: center;
  height: calc(100vh - 112px);
  width:100vw;
`

export const SignUpButton = styled.div`
  background-color: #AD1457;
  padding: 15px 55px 15px 55px;
  margin: 10px;
  color: #fff;
  border-radius: 3px;
  box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
  &:hover {
    box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
  }
`

export const AppFooter = styled.footer`
  height: 48px;
  width: 100vw;
  background-color: #B3E5FC;
`
export const FooterContent = styled(Row)`
  padding-top: 10px;
  justify-content: center;
`

