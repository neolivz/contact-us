import React from 'react'
import { Field, reduxForm } from 'redux-form'
import AriaModal from 'react-aria-modal'
import styled from 'styled-components'

const Root = styled.div`
  border: 1px solid black;
  background-color: #fff;
  padding: 30px 50px 30px 50px;
`

const Label = styled.label`
  color: #5a5a5a;
  font-size: 12px;
`

const ErrorText = styled.div`
  color:red;
  font-size: 10px;
  padding-bottom: 2em;
`

const CustomInput = styled.input`
  margin-top: 0.2em;
  margin-bottom: 0.8em;
  height: 30px;
  border: none;
  border-bottom: 1px solid #f0f0f0;
  font-size: 16px;
  width: 200px;
`
const validate = (values: Details) => {
  const errors: any = {}
  if (!values.name) {
    errors.name = 'Name is required'
  }
  if (!values.email) {
    errors.email = 'Email is required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }
  if (!values.confirmEmail) {
    errors.confirmEmail = 'Confirmation Email is required'
  } else if (values.confirmEmail !== values.email) {
    errors.confirmEmail = 'Oops!! This does not match'
  }
  return errors
}

const renderField = ({
  input,
  label,
  type,
  name,
  meta: { touched, error, warning }
}) => (
  <div>
    <div>
      <Label htmlFor={input.name}>{label}</Label>
    </div>
    <div>
      <CustomInput {...input} placeholder={label} type={type}  />
    </div>
      {touched &&
        (error && <ErrorText>{error}</ErrorText>)}
  </div>
)


let ContactForm = props => {
  const { handleSubmit } = props
  console.log(handleSubmit)
  return (
        props.dialogOpen ? (<AriaModal
          titleText="demo one"
          onExit={this.deactivateModal}
          getApplicationNode={this.getApplicationNode}
          underlayStyle={{ paddingTop: '2em' }}
        >
            {props.working ?
            <Root>Hold On</Root> :
             <form onSubmit={ handleSubmit }>
              <Root>
                <div className="modal-body">
                  <div>
                    
                  </div>
                  <div>
                    <Field name="name" component={renderField} type="text" label="Name"/>
                  </div>
                  <div>
                    <Field name="email" component={renderField} type="text" label="Email"/>
                  </div>
                  <div>
                    <Field name="confirmEmail" component={renderField} type="email" label="Confirm Email" />
                  </div>
                  
                </div>
              
                <button type="submit">
                  Submit
                </button>
              </Root>
          </form>}
      </AriaModal>) : null
    
  )
}

ContactForm = reduxForm({
  validate,
  form: 'contact'
})(ContactForm)

export default ContactForm;