import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import App from '../../components/app/App'
import {openDialog, closeDialog, openDoneDialog, closeDoneDialog, saveDetails} from '../../actions/actions'

const mapStateToProps = (state = {}) => {
	return {
		dialogOpen: state.dialog ? state.dialog.store : false,
		doneDialogOpen:  state.doneDialog ? state.doneDialog.store : false,
		working: state.details ? state.details.working : false,
		completed: state.details ? state.details.completed : false,
		failure: state.details ? state.details.failure : false
	}
}

const mapDispatchTopProps = (dispatch) => {
	return bindActionCreators({
		openDialog,
		closeDialog,
		saveDetails,
		openDoneDialog,
		closeDoneDialog
	}, dispatch)
}

export default connect(mapStateToProps, mapDispatchTopProps)(App)