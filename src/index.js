import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import store, { history } from './store'

import './index.css';
import App from './containers/app/AppContainer';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
	<Provider store={store}>
      <div>
        <App />
      </div>
  </Provider>, 
  document.getElementById('root')
);
registerServiceWorker();
