import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { TOGGLE_DIALOG, TOGGLE_DONE_DIALOG, SAVE_DETAILS_REDUCER} from '../constants'
import { createReducer, asyncReducer, syncReducer, initialState }
from 'redux-async-action-reducer'

export const dialogReducer = createReducer(initialState,
	[
		syncReducer(TOGGLE_DIALOG)
	],[]
)
export const doneDialogReducer = createReducer(initialState,
	[
		syncReducer(TOGGLE_DONE_DIALOG)
	],[]
)

export const detailsReducer = createReducer(initialState, [], [
  asyncReducer(SAVE_DETAILS_REDUCER)
])

export default combineReducers({
  details: detailsReducer,
  dialog: dialogReducer,
  doneDialog: doneDialogReducer,
  form: formReducer  
})